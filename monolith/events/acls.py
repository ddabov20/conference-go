import json, requests
from .keys import PEXEL_KEY, OPEN_WEATHER_API_KEY


def get_city_photo(city, state):
    response = requests.get(url=f"https://api.pexels.com/v1/search?query={city}, {state}", headers={
        "Authorization": PEXEL_KEY}
    )
    content = response.json()
    try:
        return content["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather(city, state):
    geocoding_params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    geocoding_url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url=geocoding_url, params=geocoding_params)

    content = response.json()
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except(KeyError, IndexError):
        return None

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }

    response = requests.get(url=weather_url, params=weather_params)

    content = response.json()
    try:
        temp = content["main"]["temp"]
        description = content["weather"][0]["description"]
    except (KeyError, IndexError):
        return None

    return {"description": description, "temp": temp}
